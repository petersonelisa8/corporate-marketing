<!-- This issue is for the social team to use only.  --> 
##### ⚠️ Issues for social media reporting are confidential 

##### 📖 [Learn more about social media reporting, including definitions, in the handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-reporting/)

##### 🔗 [Looking for OKR performance? Check out our OKR issue here](null)

## `FY2xQx` (`Month to month`) Organic Social Results 

### Summary (vs `FY2xQx`)

The following metrics are inclusive of Facebook, LinkedIn, Instagram, and Twitter from Feb 1 through Apr 30, 2021. 

| Metric      | `FY2xQx`      | `FY2xQx`      | Difference |
|-------------|-------------|-------------|------------|
| Impressions | **0**   | 0   | **+ or - x%**     |
| Engagements | **0**     | 0     | **+ or - x%**     |
| Clicks      | **0**      | 0      | **+ or - x%**    |
| Ad Value    | **$0** | $0 | **+ or - x%**    |
| Total Published Posts  | **x** | x | **a x% reduction/increase in posts**   | 

### Details

- **Metrics for social only matter in the context of all of our metrics, never alone**
   - Theoretically, we should expect a `x% reduction/increase` across the board on impressions, clicks, and engagement. Anything with worse than `-5.94%` performance required additional changes and anything with better performance or increases are due to other activities we've promoted (more below).
- **`INSERT FINDING HEADLINE HERE`**
   - `insert details about the finding`
   - `insert details about the finding`
- **`INSERT FINDING HEADLINE HERE`**
   - `insert details about the finding`
   - `insert details about the finding`
- **`INSERT FINDING HEADLINE HERE`**
   - `insert details about the finding`
   - `insert details about the finding`

##### Be sure to link this issue to the [FY Performance Epic](https://gitlab.com/groups/gitlab-com/-/epics/1465)
   
##### Links
- [Social Ad Value Calculation sheet](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit#gid=1705719415)
- [Sisense Dashboard](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels)) (data is delayed so this report may be ahead of what's in Sisense)
- [Pulled data from Sprout added to Sisense](https://docs.google.com/spreadsheets/d/1Lc3uLs7gpoYYu10cLlzqzxWCFISja1Df-FSvY_xBQU4/edit?usp=sharing)

##### Reach out to `@wspillane` directly in the comments with questions or feedback


<!-- Leave everything below this line  -->
