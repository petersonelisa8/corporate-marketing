## TRADE SHOW NAME - XX Number of expected attendees (including vendors)

**<span style="text-decoration:underline;">Swag Items with quantity and which swag vendor ordered from</span>**

**Total Qty XXXX**

- [  ] XXX Item 1 from Boundless/Chrome/Other

- [  ] XXX Item 2 from Boundless/Chrome/Other

- [  ] XXX Item 3 from Boundless/Chrome/Other

- [  ] XXX Item 4 from Boundless/Chrome/Other

- [  ] XXX Stickers from StickerMule


-----

**<span style="text-decoration:underline;">TO DO LIST</span>**

- [  ] Confirmed Orders

- [  ] Proofs Sent 

- [  ] Proofs Approved

- [  ] Vendor Shipping/Tracking Information Sent


-----

**<span style="text-decoration:underline;">## Shipping Information for [TRADE SHOW]:</span>**

Deadline to ship to warehouse: 

Shipping address for Show Warehouse:

If items can’t make it to show warehouse, alternate address to ship to: 

ATTENTION TO [Team Members Name]

Booth Number (If Applicable)

Copy and paste Google Sheet link with shipment tracking spreadsheet 
