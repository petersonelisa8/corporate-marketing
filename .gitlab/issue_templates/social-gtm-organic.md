#### Request for GTM content to be shared on owned organic social channels.
<!-- **** This issue template is for the corporate events marketing team to use in aiding with a social campaign. If the event is not attached to corporate marketing, we won't be able to support on our brand social channels. -->

_Each piece of content should get its own issue. These issues are intended to be attached to GTM Tactic child epics pre-completion by the campaign manager. Once the work is completed, the social team will reassociate this issue with social GTM child epic for record keeping._

### 📬 STEP 1: For Requester from Campaign Team  <!-- Requester please fill in all **[sections]** above the solid line for STEP 1. Please do not assign the issue to the social team until you're ready to answer the questions. If the social team still needs more info, they'll ask in the comments. Note that these questions are critical for understanding your request. -->

##### What is your request all about?

`add content-specific/unique details here. consider linking to an epic/issue, a Google doc, or other items that may be necessary to understand before fulfilling the request. If the request has a different objective, audience, or other parameters (UTM), than the campaign child epic, describe those differences here. You do **not** need to duplicate information included in the organic social child-epic for this GTM campaign.`

##### Add appropriate link(s) for user journey; Consider required creative. Please be sure consider UTM tracking of owned links.
- [ ] `add description` - `add link here`
- [ ] `add description` - `add link here`

##### Creative assets are required; sometimes are included with the URL to promote via a social card 
_Creative elements, like images or videos, are required for sharing on organic social channels. [Check links using this social card validator](https://cards-dev.twitter.com/validator). If there are no creative assets, including no opengraph card attached to the URL link we'll be promoting, social cannot fulfill this request._

* [ ]  There is an existing social card/opengraph attached to the link(s)
* [ ]  Existing assets are here: `insert link to related issue or repo for creative`
* [ ]  I require new custom assets for social (requires working with brand design team)
* [ ]  I'm not sure what I require (the social team will review with you in the comments below)

#####   Pertinent Dates
- [ ] This is an event that supports a GTM, like a webinar

Content launch date (For events, this is the day it happens): `x/xx/xx` 

Select one of the following:

- [ ] Promotion needs to be started by: `x/xx/xx` ➡️ _add as the due date to the right_
- [ ] Promote when calendar space is available

#### Are you considering paid social advertising for this content?

* [ ]  Yes, I'm requesting paid social advertising `add paid social request as a related issue`
* [ ]  No, I am not requesting paid social advertising

_Note that organic social content should only support social awareness and engagement stages of the funnel and that other, down funnel content should have a budget and paid social promotion._

#### Campaign Manager To-Do's
- [ ] Add this issue to the GTM Tactic epic
- [ ] Confirm all of the questions are answered before assigning the social team

---

### Step 2: Social Team: To-Do's

* [ ]  Review details above and begin drafting brand posts
* [ ]  Asset Review: if asset links were provided, please review to be appropriate for social. If existing assets are not appropriate or if the requester asked for custom assets, please open a design issue, tag requester, and link as a related issue here.

### ✍️ STEP 3: All: Drafts, suggestions, and schedule 
*Please work on these items in the comments below.*

### 🗓 STEP 4: Social Team: Scheduled posts

* [ ]  All posts that can be added to Sprout are scheduled
* [ ]  Advocacy: Be sure to as 1 post / 2-3 copy suggestions per channel, to support this initiative
* [ ]  This issue requires live community management coverage. `[insert social team member handle]` will be actively scheduled during `[window of time]`.
* [ ]  Add this issue to the associated social/gtm epic, removing it from the GTM tactic epic

/label ~"Social Media" ~"Corp Comms" ~"Corporate Marketing" ~"usecase-gtm" ~"Stage::Awareness"

/assign @social

/milestone %"Organic Social: Triage"
