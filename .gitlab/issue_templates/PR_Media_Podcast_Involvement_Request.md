## Request Details

* For which event: 
* Epic: please relate issue back to the Event Epic
* Campaign tag if needed:

## :girl: Who 
* `Who are we trying to bring to the event?` 
* `Contact Info for Guest(s)`
* `Who will be onsite DRI to host this guest?`

## :trophy: Purpose/ Reasoning/ Goals:
* `Please add in value this will bring to event/ driing goals.`

## :moneybag: Cost/ Payment
* `What fees are involved? Will we be paying for their travel? Will we need to pay for setup? Power? Internet? AV?`
* `Have they been setup in tilapi? DRI will be responsible for setting up billing with vendor and setting budgeting expectations with planning team`
* `What team/ project is thing being billed to?`

## :microphone: Onsite Requirements: Space, Time, Special Arrangements....
* `Does guest need private space? What other needs or requiremnent must be met for them onsite?` 

## :package: Shipping
* `Will they be shipping anythigng to the venue?`

## :paperclip: Anything else planning team needs to know?



/assign @lconway @Emily

/label ~Events ~"Corporate Event" ~PR

