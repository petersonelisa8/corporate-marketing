<!-- This issue is for the social team to use only.  --> 
##### ⚠️ Issues for social media reporting are confidential 

##### 🔗 [Learn more about social media reporting, including definitions, in the handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-reporting/)

_Specifics will change based on what we're reporting on. The outline below covers all available items, but some may not be relevant in the final issue._

## `Insert what title of what we're reporting on` 

### Summary
`insert a short summary of what we're reporting on, if we feel it was successful, and any learnings.`

- Impressions: `insert number`
- Engagements: `insert number`
- Link clicks: `insert number`
- Ad Equivalency: `$insert number`

- Example Post 1 `Attach a link here`
- Example Post 2 `Attach a link here`

##### Next steps: 
- [ ] continuing campaign or adding more posts to calendar
- [ ] ending campaign or not adding more posts to calendar
- [ ] revising copy, visuals, or other assets before moving forward
- [ ] documenting learnings in a deck, handbook page, or other location

##### List this issue in the [FY Performance Epic description](https://gitlab.com/groups/gitlab-com/-/epics/1465)

Full report: `Attach a link or pdf of a report here`

##### Reach out to `insert your handle` directly in the comments with questions or feedback

### Details
`add more detail on our reporting here --- What did we learn? Anything interesting happen? Did we get user feedback? Consider adding insights to this section` 

<!-- Leave everything below this line  --> 

/label ~"Social Media" ~"Corp Comms"~"Corporate Marketing" ~"mktg-status::plan"

/assign @social

/milestone %"Organic Social: Triage"

/confidential
