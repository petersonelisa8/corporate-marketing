



# E-Group External Communications Request Issue Template

Template: linked in Handbook


## Details



* What’s the opportunity?
    * Media interview, podcast appearance, speaking opportunity (i.e. conference, live streamed fireside chat, etc), video request, contributed article or guest blog.
    * Please check below and fill out all relevant information requested. 

- [ ] :movie_camera: **Video Requests**



* When is the opportunity? 
* When will this be published?
* What type of broadcast or medium? (link)
* Where will the content live after?
* What’s the topic?
    * Proposed video script
* Who is the audience?
    * Does the audience include business decision-makers?
* What is the promotion plan for the video (include proposed impressions)?
* Video Format?
* Will there be other speakers in the video? If so, who?
* Who is producing the video? 
* Organization Making the Request (Is there a strategic fit? Are we members of the organization or have other affiliations?)
* Business Development/Reputation Management (Does the opportunity offer a compelling business development opportunity? Will the video help enhance our corporate reputation?)
* Geography (Is the opportunity focused in a region where we want to establish or strengthen GitLab’s presence?)

- [ ] :pencil2: **Contributed Article or Guest Blog**



* Outlet name, link 
* When is the opportunity? 
* When will this be published?
* What’s the topic?
    * Include outline
* Who is the audience?
    * Does the audience include business decision-makers?
* Proposed impressions/metrics
* Organization Making the Request (Is there a strategic fit? Are we members of the organization or have other affiliations?)
* Business Development/Reputation Management (Does the opportunity offer a compelling business development opportunity? Will the content help enhance our corporate reputation?)
* Geography (Is the opportunity focused in a region where we want to establish or strengthen GitLab’s presence?)

- [ ] :newspaper:  Media Interviews and Podcast Appearances



* When is the opportunity? 
* When will this air? 
* Outlet name, link and interviewer name and bio link
* What type of broadcast or medium?
* What’s the topic?
* Who is the audience?
    * Does the audience include business decision-makers?
* Proposed impressions/metrics
* Does this fit into the executives elevation strategy and narrative pillars?
* Business Development/Reputation Management (Does the opportunity offer a compelling business development opportunity? Will the content help enhance our corporate reputation?)
* Geography (Is the opportunity focused in a region where we want to establish or strengthen GitLab’s presence?)

- [ ] :microphone: **Speaking Opportunities**



* When is the event? 
* Is this an in-person or virtual speaking engagement? 
    * If virtual, is this a live speaking engagement or submitted recording? 
    * If submitted, when is the recording due? 
* Event link and focus? 
* Who is the audience?
    * Does the audience include business decision-makers?
* Who are the other speakers?
    * Are the other speakers of the same level as the exec you are requesting to speak (i.e. CEOs and founders if requesting Sid)
* What type of speaking engagement (keynote, breakout session w/slides, luncheon address, fireside chat, panel discussion)?
    * If a panel, is it a panel of peers? 
    * If a panel, how many are participating?
* Are any competitors placed in a better speaking slot than our executive?
* Will there be media or analysts attending?
* Where will the content live after?
* Has the proposed speaker done this event before?
* Has GitLab previously been involved with this event?
* Organization Making the Request (Is there a strategic fit? Are we members of the organization or have other affiliations? If an event, who are the sponsors of the event?
* Business Development/Reputation Management (Does the opportunity offer a compelling business development opportunity? Will the forum help enhance our corporate reputation?)
* Geography  (Is the opportunity focused in a region where we want to establish or strengthen GitLab’s presence?
* Repackaging (Will the speaking opportunity lend itself to repackaging as an op-ed, feature piece, or a spur for media interviews, posting on YouTube or social media


## Criteria



* More than three weeks out (y/n)
* Significant business impact (y/n) Include justification


## Business Justification



* What’s the business case? _(If you are requesting a non E-Group member, please follow the link to [general speaking guidelines](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications/#speaking-on-behalf-of-gitlab-guidelines-for-media--podcasts-conferences--meetups-social-media-blogs-research-books-and-in-gitlab-issues), this template is only for E-Group speaking requests). _
* What is your justification for having this E-Group members participate? (i.e. they are the only ones who are designated spokespeople for the company, they personally worked on this project, they have a personal relationship with the other speakers/orgs, etc.) 
    * If this falls into another category and none of the above, there’s a high chance that we will recommend someone else to speak that is not an E-Group member 
* What’s the return on investment (ROI)? Participating in this opportunity would would [fill in reason]
* Why will you not achieve this ROI with any other company speaker?


## Logistics



* When is the event/ opportunity?
* When do materials need to be sent to the meeting organizers?
* Any other deadlines to be aware of?


## Your Checklist



* Your responsibility: You are agreeing to the following responsibilities you will take on for this event. 
    * Fill out all above questions with detailed information.
    * If any information is missed, you will reach out to the organization and obtain all missing information. 
    * If Corp Comms, Legal or E-group has additional questions, you will reach out to the organization and obtain answers to said questions.
    * Content - Beyond the topic area, you may need to provide script outline, talking points, customer quotes/approvals, analyst quotes/approvals, facts and figures (with documentation for legal), and/or images and graphs depending on the specifics of the opportunity.
* If an E-group member agrees, [Corp Comms Team] will take these responsibilities. 
    * Scheduling and other logistical needs between EBA and external organization
    * Collaborating with requester on content creation
    * Coordinating reviews with legal
    * Practice and prep sessions with e-group member
    * Amplification of opportunity




















<!-- Please leave the label below on this issue -->
/label ~"Corporate Marketing" ~"mktg-status::plan" 
